"use strict";

const passForm = document.querySelector(".password-form");
const pass = document.getElementById("password");
const passConfirm = document.getElementById("password_confirm");
const submit =  document.querySelector(".btn"); 
const paragraph = document.querySelector(".paragraph");
const icon = document.getElementById("icon");
const iconConfirm = document.getElementById("icon-confirm");

submit.addEventListener("click", e => {
    pass.value === passConfirm.value ? (paragraph.textContent = "", alert("You're welcome")) : 
                                        paragraph.textContent = "Нужно ввести одинаковые значения";
});

function showPass(){
    icon.classList.toggle("fa-eye-slash");
    icon.classList.toggle("fa-eye");
    switch(pass.type){
        case "text": pass.type = "password";
        break;
        case "password": pass.type = "text";
        break;
    };
};

function showPassConfirm(){
    iconConfirm.classList.toggle("fa-eye-slash");
    iconConfirm.classList.toggle("fa-eye");
    switch(passConfirm.type){
        case "text": passConfirm.type = "password";
        break;
        case "password": passConfirm.type = "text";
        break;
    }
}
